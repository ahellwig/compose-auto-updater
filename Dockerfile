FROM docker:latest
ENV TARGET_DIR=/target/
ENV SLEEP_DUR=60
ADD update.sh /
RUN chmod +x /update.sh
ENTRYPOINT /update.sh

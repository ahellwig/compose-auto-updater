# compose-auto-updater

Automatically updates running docker-compose containers with the latest images.

## Usage
```
docker run -d --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /path/to/compose-project:/target registry.gitlab.com/ahellwig/compose-auto-updater
```

## Options
Set the environment variables
- `TARGET_DIR`: the compose-project dir in the container(default: `/target`)
- `SLEEP_DUR`: the duration(in seconds) between two updates

Default is equivalent to:
```
docker run -e TARGET_DIR=/target -e SLEEP_DUR=60 -d --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /path/to/compose-project:/target registry.gitlab.com/ahellwig/compose-auto-updater
```
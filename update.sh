#!/bin/sh
if ! [ -d "$TARGET_DIR" ]; then
    echo "Can not find target in $TARGET_DIR. Aborting!"
    exit 1
fi

[ -z "$DEBUG" ] || echo "working in $TARGET_DIR"
cd "$TARGET_DIR"
hash=$(md5sum "$TARGET_DIR/docker-compose.yml" | awk '{ print $1 }')
while true; do
    # process changes of docker-compose.yml
    newHash=$(md5sum "$TARGET_DIR/docker-compose.yml" | awk '{ print $1 }')
    [ -z "$DEBUG" ] || echo "hash: $hash newHash: $newHash"
    [ "$hash" == "$newHash" ] && echo "docker-compose.yml stayed the same" || docker-compose --no-ansi down
    hash=$(md5sum "$TARGET_DIR/docker-compose.yml" | awk '{ print $1 }')

    # process changes on images
    docker-compose --no-ansi pull
    docker-compose --no-ansi up -d --build

    sleep $SLEEP_DUR
done
